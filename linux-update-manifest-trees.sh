#!/bin/bash

# $1: git repo url
# $2: remote name
# $3: directory
clone_or_update()
{
  git_repo="$1"
  remote="$2"
  directory="$3"
  if [ ! -d "${directory}" ]; then
    git clone -o "${remote}" "${git_repo}" "${directory}" > /dev/null
  else
    pushd "${directory}" > /dev/null || exit 1
    if ! git remote -v | grep -q "^${remote}\s"; then
      git remote add "${remote}" "${git_repo}" > /dev/null
    fi
    git fetch "${remote}" > /dev/null 2>&1
    popd > /dev/null || exit 1
  fi
}

GIT_TREES=(
  linux-stable-rc#https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
  linux-stable#https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
  torvalds#https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  linux-next#https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
)

kernel_dir=/data/linux
root_dir="$(dirname "$(readlink -e "$0")")"
now="$(date -u +%Y%m%d-%H%M%S)"

cd "${root_dir}" || exit 1

# Check manifest first;
# if manifest is different, verify

wget -qN https://git.kernel.org/manifest.js.gz

if [ -s kernelorg-trigger/manifest.js.gz ]; then
  if diff -Naur <(md5sum manifest.js.gz | awk '{print $1}') <(md5sum kernelorg-trigger/manifest.js.gz | awk '{print $1}') > /dev/null; then
    # echo "Current manifest is the same as the archive. Exiting."
    exit 0
  fi
fi

clone_or_update git@gitlab.com:mrchapp/kernelorg-trigger.git gitlab kernelorg-trigger

pushd kernelorg-trigger > /dev/null || exit 1
git checkout -B auto gitlab/auto > /dev/null 2>&1

cp -p "${OLDPWD}/manifest.js.gz" .
cp -p manifest.js.gz "manifest-${now}.js.gz"

if [ "$(git status -s manifest.js.gz)" = "" ]; then
  # echo "No changes in the manifest. Exiting."
  exit 0
fi

clean="true"
for tree in ${GIT_TREES[*]}; do
  name="${tree%%#*}"
  url="${tree#*#}"

  url_stripped="${url/https:\/\/git.kernel.org/}"

  fingerprint=$(zcat manifest.js.gz | jq -r ".[\"${url_stripped}\"] | .fingerprint")
  timestamp=$(zcat manifest.js.gz | jq -r ".[\"${url_stripped}\"] | .modified")

  signature="${timestamp},${fingerprint}"
  if [ ! -f "${name}" ] || ! grep -q "${signature}" "${name}"; then
    clean="false"
    echo "Need to trigger ${name}"
    echo "${timestamp},${fingerprint}" | tee "${name}"
    git add "${name}"

    # Poor man's mirroring
    git -C "${kernel_dir}" fetch "${name}"
    case "${name}" in
      linux-stable-rc)
        # stable branches
        # for b in 5.18 5.15 5.10 5.4 4.19 4.14 4.9; do git -C "${kernel_dir}" branch -f linux-${b}.y-rc linux-stable-rc/linux-${b}.y; done
        for b in 5.18 5.15 5.10 5.4 4.19 4.14 4.9; do git -C "${kernel_dir}" push mirror-stable-rc +linux-stable-rc/linux-${b}.y:linux-${b}.y; done
        for b in 5.18 5.15 5.10 5.4 4.19 4.14 4.9; do
          tag="$(git -C "${kernel_dir}" describe --exact-match "linux-stable-rc/linux-${b}.y-rc" 2>/dev/null ||:)"
          if [ -n "${tag}" ]; then
            git -C "${kernel_dir}" push mirror-stable-rc "${tag}"
          fi
        done
        # queues
        # for b in 5.18 5.15 5.10 5.4 4.19 4.14 4.9; do git -C "${kernel_dir}" branch -f queue/${b} linux-stable-rc/queue/${b}; done
        for b in 5.18 5.15 5.10 5.4 4.19 4.14 4.9; do git -C "${kernel_dir}" push mirror-queue +linux-stable-rc/queue/${b}:queue/${b}; done
        ;;
      torvalds)
        # git -C "${kernel_dir}" branch -f master torvalds/master ||:
        git -C "${kernel_dir}" push mirror-torvalds torvalds/master:master
        tag="$(git -C "${kernel_dir}" describe --exact-match torvalds/master 2>/dev/null ||:)"
        [ -n "${tag}" ] && git -C "${kernel_dir}" push mirror-torvalds "${tag}" ||:
        ;;
      linux-next)
        # git -C "${kernel_dir}" branch -f next-master linux-next/master
        git -C "${kernel_dir}" push mirror-next +linux-next/master:master
        tag="$(git -C "${kernel_dir}" describe --exact-match linux-next/master 2>/dev/null ||:)"
        [ -n "${tag}" ] && git -C "${kernel_dir}" push mirror-next "${tag}" ||:
        ;;
    esac
  fi
done

if [ "${clean}" = "false" ]; then
  git add -u .
  git commit -s -m "Autoupdate from ${now}"
  git push gitlab auto
fi
popd > /dev/null || exit 1
